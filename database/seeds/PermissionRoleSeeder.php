<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $super_admin = Role::create(['name' => 'super-admin', 'display_name' => 'Super Admin']);
        $manager     = Role::create(['name' => 'manager', 'display_name' => 'Manager']);
        $super_visor = Role::create(['name' => 'supervisor', 'display_name' => 'Super Visor']);
        $staff       = Role::create(['name' => 'staff', 'display_name' => 'Staff']);
    }
}
