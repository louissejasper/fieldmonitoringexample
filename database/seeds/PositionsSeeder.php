<?php

use App\Models\Position;
use App\Models\Unit;
use Illuminate\Database\Seeder;

class PositionsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Unit::find(1)->attachByPosition(new Position(['slug' => 'developer', 'name' => 'Developer']));
        Unit::find(1)->attachByPosition(new Position(['slug' => 'developer', 'name' => 'Head Developer', 'is_head' => "manager"]));
        Unit::find(2)->attachByPosition(new Position(['slug' => 'sales-manager', 'name' => 'Sales Manager']));
        Unit::find(2)->attachByPosition(new Position(['slug' => 'sales-manager', 'name' => 'Head Sales Manager', 'is_head' => "manager"]));
        Unit::find(3)->attachByPosition(new Position(['slug' => 'project-manager', 'name' => 'Project Manager']));
        Unit::find(3)->attachByPosition(new Position(['slug' => 'project-manager', 'name' => 'Head Project Manager', 'is_head' => "manager"]));
        Unit::find(4)->attachByPosition(new Position(['slug' => 'creative-designer', 'name' => 'Creative Designer']));
        Unit::find(4)->attachByPosition(new Position(['slug' => 'creative-designer', 'name' => 'Head Creative Designer', 'is_head' => "manager"]));
        Unit::find(5)->attachByPosition(new Position(['slug' => 'search-engine-specialist', 'name' => 'Search Engine Specialist']));
        Unit::find(5)->attachByPosition(new Position(['slug' => 'search-engine-specialist', 'name' => 'Head Search Engine Specialist', 'is_head' => "manager"]));
        Unit::find(5)->attachByPosition(new Position(['slug' => 'content-writer', 'name' => 'Content Writer']));
        Unit::find(5)->attachByPosition(new Position(['slug' => 'content-writer', 'name' => 'Head Content Writer', 'is_head' => "manager"]));
        
    }
}
