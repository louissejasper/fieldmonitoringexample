<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Factory::create();
        User::create([
            'name'           => 'Vincent',
            'email'          => 'admin@gmail.com',
            'password'       => bcrypt('password'),
            'remember_token' => str_random(10),
            'firstname'      => 'Vincent',
            'lastname'       => 'Dela Fuent',
            'emp_id'         => '0168',
            'unit_id'        => 1,
            'position_id'    => 1,
            'contact_number' => $faker->phoneNumber,
            'image'          => $faker->word,
        ]);
        factory(User::class, 50)->create();
    }
}
