<?php

use App\Models\Shift;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ShiftsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $shifts = [
            ['start'=>'N','end'=>'A'],
            ['start'=>'7:30 AM','end'=>'4:30 PM'],
            ['start'=>'8:00 AM','end'=>'5:00 PM'],
            ['start'=>'8:30 AM','end'=>'5:30 PM'],
            ['start'=>'9:00 AM','end'=>'6:00 PM'],
            ['start'=>'9:30 AM','end'=>'6:30 PM']
        ];
        foreach($shifts as $shift){
            Shift::create($shift);
        }
    }

}
