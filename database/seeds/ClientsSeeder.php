<?php

use App\Models\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;


class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();
        	factory(Client::class,30)->create();
        Model::reguard();
    }
}
