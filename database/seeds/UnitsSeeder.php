<?php

use App\Models\Unit;
use Illuminate\Database\Seeder;

class UnitsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $units = [
            ["slug" => 'development', 'name' => 'Development'], //1
            ["slug" => 'strategic-sales-manager', 'name' => 'Strategic Sales Manager'], //2
            ["slug" => 'project-management', 'name' => 'Project Management'], //3
            ["slug" => 'creative-design', 'name' => 'Creative Design'], //4
            ["slug" => 'online-content-marketing', 'name' => 'Online Content Marketing'], //5
            ["slug" => 'support-and-engineering', 'name' => 'Support and Engineering'], //6
        ];

        foreach ($units as $unit) {
            Unit::create($unit);
        }
    }
}
