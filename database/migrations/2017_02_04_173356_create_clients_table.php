<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::enableForeignKeyConstraints();
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string("whmcs_id")->nullable();
            $table->text("company")->nullable();
            $table->string("contact_person", 100)->nullable();
            $table->string("contact_number", 100)->nullable();
            $table->text("address")->nullable();
            $table->string("email", 100)->nullable();//->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clients');
    }
}
