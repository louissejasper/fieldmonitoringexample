<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAppointmentTable extends Migration {

    const APPOINTMENTS          = 'appointments';
    const APPOINTMENT_WITH      = 'appointment_with';
    const APPOINTMENT_HISTORIES = 'appointment_histories';
    const APPOINTMENT_NOTIFY    = 'appointment_notify';
    const USER                  = 'users';
    const CLIENTS               = 'clients';
    const SHIFTS                = 'shifts';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::enableForeignKeyConstraints();
        /*Foreignkey assignment*/
        Schema::table(self::APPOINTMENTS, function (Blueprint $table) {
           /* $table->foreign('client_id')->references('id')->on(self::CLIENTS);
            $table->foreign('shift_id')->references('id')->on(self::SHIFTS);*/
        });

        if (!Schema::hasTable(self::APPOINTMENT_WITH)) {
            Schema::create(self::APPOINTMENT_WITH, function (Blueprint $table) {
                $table->integer("appointment_id")->unsigned();
                $table->integer("user_id")->unsigned();
                $table->dateTime("viewed")->nullable();
                $table->boolean('owner')->default(false);
                $table->timestamps();
            });
            Schema::table(self::APPOINTMENT_WITH, function (Blueprint $table) {
                /*$table->foreign('appointment_id')->references('id')->on(self::APPOINTMENTS);
                $table->foreign('user_id')->references('id')->on(self::USER);*/
            });
        }

        if (!Schema::hasTable(self::APPOINTMENT_HISTORIES)) {
            Schema::create(self::APPOINTMENT_HISTORIES, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('appointment_id')->unsigned();
                $table->string('status')->nullable();
                $table->text('modification')->nullable();
                $table->timestamps();
            });

            Schema::table(self::APPOINTMENT_HISTORIES, function (Blueprint $table) {
                // $table->foreign('appointment_id')->references('id')->on(self::APPOINTMENTS);
            });

        }

        /*Notification for the mancom*/
        if (!Schema::hasTable(self::APPOINTMENT_NOTIFY)) {
            Schema::create(self::APPOINTMENT_NOTIFY, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('appointment_id')->unsigned();
                $table->integer('manager_id')->unsigned();
                $table->dateTime('viewed')->nullable();
                $table->timestamps();
            });

            Schema::table(self::APPOINTMENT_NOTIFY, function (Blueprint $table) {
                /*
                Di ko alam kung bakit di ito gumaga further reasearch muna pa
                $table->foreign('appointment_id')->on('id')->references(self::APPOINTMENTS);
                $table->foreign('manager_id')->on('id')->references(self::USER);
                */
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();

        Schema::table(self::APPOINTMENTS, function (Blueprint $table) {
           /* $table->dropForeign(['client_id']);
            $table->dropForeign(['shift_id']);*/
        });

        Schema::table(self::APPOINTMENT_WITH, function (Blueprint $table) {
            /*$table->dropForeign(['appointment_id']);
            $table->dropForeign(['user_id']);*/
        });

        Schema::dropIfExists(self::APPOINTMENT_WITH);

        Schema::table(self::APPOINTMENT_HISTORIES, function (Blueprint $table) {
            /*$table->dropForeign(['appointment_id']);
            $table->dropForeign(['manager_id']);*/
        });

        Schema::dropIfExists(self::APPOINTMENT_HISTORIES);

        Schema::table(self::APPOINTMENT_NOTIFY, function (Blueprint $table) {
            /*$table->dropForeign(['appointment_id']);
            $table->dropForeign(['manager_id']);*/
        });

        Schema::dropIfExists(self::APPOINTMENT_NOTIFY);

    }
}