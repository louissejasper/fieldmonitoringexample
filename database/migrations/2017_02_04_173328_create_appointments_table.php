<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::enableForeignKeyConstraints();
        Schema::create('appointments', function (Blueprint $table) {
            // $table->();
            $table->increments('id');
            $table->integer("client_id")->unsigned()->nullable();
            $table->integer("shift_id")->unsigned()->default(1)->nullable();
            $table->string("txn", 32)->nullable();
            $table->datetime("schedule")->nullable();
            $table->text("location")->nullable();
            $table->string("purpose", 20)->nullable();
            $table->date("date_start")->nullable();
            $table->date("date_end")->nullable();
            $table->time("departure")->nullable();
            $table->time("arrival")->nullable();
            $table->text("remark")->nullable();
            $table->string("status")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('appointments');
    }
}
