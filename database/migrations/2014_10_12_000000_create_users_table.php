<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::enableForeignKeyConstraints();

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email'); //->unique();
            $table->string('password');
            $table->integer("emp_id")->nullable();
            $table->integer('unit_id')->unsigned()->nullable();
            $table->integer('position_id')->unsigned()->nullable();
            $table->string("firstname", 30)->nullable();
            $table->string("lastname", 40)->nullable();
            $table->string("contact_number", 30)->nullable();
            $table->string("image", 40)->nullable();
            $table->text("address")->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });

        // Create table for storing permissions
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('group')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('permission_id')->references('id')->on('permissions')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });

        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120)->nullable();
            $table->string('slug', 120)->nullable();
            $table->timestamps();
        });

        /*Schema::create('unit_user', function (Blueprint $table) {
            $table->integer('unit_id')->nullable();
            $table->integer('user_id')->nullable();
        });*/

        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_id')->unsigned();
            $table->string('name', 100)->nullable();
            $table->string('slug', 100)->nullable();
            $table->string('is_head')->nullable();
            $table->timestamps();
        });

        /*Schema::create('position_user', function (Blueprint $table) {
            $table->integer('position_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
        });*/

        /*Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->string('slug', 100)->nullable();
            $table->timestamps();
        });*/

        /*Schema::create('position_unit', function (Blueprint $table) {
            $table->integer('position_id')->unsigned()->index();
            $table->integer('unit_id')->unsigned()->index();
            $table->boolean('is_head')->default(0);
        });*/

      

        /*Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('position_id')->unsigned()->index();
            $table->integer('unit_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->boolean('is_head')->default(0);
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::dropIfExists('roles');
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('permissions');
        Schema::dropIfExists('permission_role');
        Schema::dropIfExists('units');
        // Schema::dropIfExists('unit_user');
        Schema::dropIfExists('positions');
        // Schema::dropIfExists('position_unit');
        // Schema::dropIfExists('position_user');
        // Schema::dropIfExists('organizations');
    }
}
