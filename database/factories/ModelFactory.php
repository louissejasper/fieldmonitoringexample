<?php



/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */
$positions = App\Models\Position::get();
$factory->define(App\User::class, function (Faker\Generator $faker) use ($positions) {
    static $password;
    $position = $positions->random(1)->first();
    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'firstname'      => $faker->firstname,
        'lastname'       => $faker->lastname,
        'emp_id'         => $faker->randomNumber(),
        'contact_number' => $faker->phoneNumber,
        'image'          => $faker->word,
        'address'        => $faker->address,
        'position_id'   => $position->id,
        'unit_id'       => $position->unit_id
    ];
});

$factory->define(App\Models\Client::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'whmcs_id'       => $faker->unique()->randomNumber(),
        'company'        => $faker->company,
        'contact_person' => $faker->name,
        'contact_number' => $faker->phoneNumber,
        'address'        => $faker->address,
        'email'          => $faker->unique()->companyEmail,
    ];
});
