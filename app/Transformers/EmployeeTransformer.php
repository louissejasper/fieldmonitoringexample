<?php

namespace App\Transformers;

use App\Models\Employee;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract {

    public function transform(Employee $employee) {
    	
        return [
            'name'      => $employee->name,
            'firstname' => $employee->firstname,
            'position'  => $employee->lastname,
            'unit'      => $employee->units->name,
            'employee'  => $employee->emp_id,
            'email'     => $employee->email,
        ];
    }
}