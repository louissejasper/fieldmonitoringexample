<?php

namespace App\Observers;

use App\Models\Appointment;
use App\Models\AppointmentHistory;
use Carbon\Carbon;
use Endroid\QrCode\QrCode;
use Illuminate\Http\Request;

class AppointmentObservers {

    protected $appointmentHistoriesTbl = 'appointment_histories';

    protected $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function creating(Appointment $appointment) {
        $appointment->txn    = $this->generateTxn();
        $appointment->status = "Pending";
    }

    /**
     * { Generate QrCode }
     *
     * @param \App\Models\Appointment $appointment The appointment
     */
    public function created(Appointment $appointment) {
        $this->generateQrCode($appointment);
        $this->histories($appointment);
    }

    public function updating(Appointment $appointment) {
        $this->histories($appointment);
    }

    private function histories($appointment) {
        //insert status
        // get the updated version of the request
        // modification to json
        // $appointmentUpdates =
        // Todo Use this Documented class for polymorphic relation
        // procedural muna
        $documented = [
            'modification' => $this->request->except(['_token', '_method', 'others']),
        ];
        $history = new AppointmentHistory([
            'status'       => $appointment->status,
            'modification' => json_encode($documented),
        ]);
        $appointment->histories()->save($history);
    }

    private function generateQrCode(Appointment $appointment) {
        $qrCode = (new QrCode())
            ->setText($appointment->txn)
            ->setSize(300)
            ->setPadding(10)
            ->setErrorCorrection('high')
            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
            ->setLabelFontSize(16)
            ->setLabel('iConcept Global Advertising')
            ->setImageType(QrCode::IMAGE_TYPE_PNG)
            ->setLogo('images/iconcept-qrcode-logo.png')
            ->setLogoSize(120);
            
        $qrCode->render(__('appointment.storage') . "/$appointment->txn.png");
        // Storage::disk('public')->put("qrcode/$appointment->txn.png",$qrCode->get());
    }

    private function generateTxn() {
        $txn = Carbon::now();
        return hash('haval128,3', bcrypt($txn->toDateTimeString() . '.' . uniqid()), false);
    }
}