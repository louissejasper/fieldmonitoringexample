<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider {
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        
        /*app('Dingo\Api\Auth\Auth')->extend('basic', function ($app) {
           return new Dingo\Api\Auth\Provider\Basic($app['auth'], 'email');
        });*/
        
        /*$app['Dingo\Api\Auth\Auth']->extend('oauth', function ($app) {
            return new Dingo\Api\Auth\Provider\JWT($app['Tymon\JWTAuth\JWTAuth']);
        });

        $app['Dingo\Api\Http\RateLimit\Handler']->extend(function ($app) {
            return new Dingo\Api\Http\RateLimit\Throttle\Authenticated;
        });

        $app['Dingo\Api\Transformer\Factory']->setAdapter(function ($app) {
            $fractal = new League\Fractal\Manager;
            $fractal->setSerializer(new League\Fractal\Serializer\JsonApiSerializer);
            return new Dingo\Api\Transformer\Adapter\Fractal($fractal);
        });*/
        // $app->register(Dingo\Api\Provider\LumenServiceProvider::class);
    }
}
