<?php

namespace App;

use App\Models\Unit;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable {
    use Notifiable, CanResetPassword, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'firstname', 'lastname', 'emp_id', 'contact_number', 'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function appointment() {
        return $this->belongsTomany(Appointment::class, 'appointment_with', 'member_id', 'appointment_id');
    }

    public function groups() {
        return $this->belongsTomany(Unit::class, 'unit_user', 'unit_id', 'user_id');
    }

    // public function __call($name,$args = []) {
    //     if (method_exists($this, 'position')) {
    //         $this->position();
    //     }
    // }
    
}
