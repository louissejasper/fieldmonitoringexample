<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;

class PositionsController extends Controller {

	
	public function getPositions(Request $request,Unit $units){
		return response()->json($units->positions()->select('id','name')->get());
	}
 
}
