<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Unit;
use Illuminate\Http\Request;

class EmployeesController extends Controller {

    
    protected $employee;

    protected $request;
    /*future use*/
    public function __construct(Request $request, Employee $employee) {
        $this->employee = $employee;
        $this->request  = $request;
    }

    public function index() {
        $employee = Employee::paginate(10);
        return view('pages.employees.lists')
            ->with([
                'employees' => $employee,
            ]);
    }

    public function create(Employee $employee, Unit $unit) {

        return view('pages.employees.create')
            ->with([
                'employee' => $employee,
                'unit'     => $unit,
            ]);
    }

    public function preview(Request $request, Employee $employee) {
        return view('pages.employees.preview')
            ->with([
                'employee' => $employee,
            ]);
    }

    public function store(Request $request, Employee $employee) {
        $data               = $request->except(['_token', '_method']);
        $employee->password = bcrypt(request()->has('password') ?: 'password');
        $employee->fill($data)->save();
        return redirect()->route('employees.preview', ['employee' => $employee]);
    }

    public function edit(Employee $employee, Unit $unit) {
        return view('pages.employees.update')
            ->with([
                'employee' => $employee,
                'unit'     => $unit,
            ]);
    }

    public function update(Request $request, Employee $employee) {
        $data = $request->except(['_token', '_method']);
        $employee->fill($data)->save();
        return redirect()->route('employees.preview', ['employee' => $employee]);
    }

    public function delete(Request $request, Employee $employee) {
        $employee->delete();
        return redirect()->route('employees.list');
    }

}
