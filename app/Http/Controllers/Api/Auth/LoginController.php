<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Transformers\EmployeeTransformer;
use App\Transformers\UserTransformer;
use Auth;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller {

	use Helpers;

    public function authenticate(Request $request) {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $employee = Employee::find(Auth::id());
        // all good so return the token
        return $this->response->item($employee, new EmployeeTransformer)
            ->addMeta('token', $token)
            ->addMeta('token_type', 'Bearer');

    }
}
