<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\Employee;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;

class MeetingsController extends Controller {
    
    protected $response;

	public function __construct(Response $response){
		$this->response = $response;
	}

	public function lists(){
		return Appointment::get();
	}

	public function getEmployees(Employee $employee){
		return $employee;
	}

}
