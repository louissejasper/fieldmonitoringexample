<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        // $user    = JWTAuth::parseToken()->toUser();
        return Employee::paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
         
    }


    public function retrieve(Request $request,$id){
        try {
            $user = Employee::findOrFail($id);
            return $user;
        } catch (Exception $e) {
            response()->json(['status'=>false,"message"=>"No User find"]);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->except(['created_at','deleted_at','updated_at','id']);
        $employee = new Employee();
        $employee->password = bcrypt("password");
        $employee->name = sprintf('%s %s',$request->input('lastname'),$request->input('lastname'));
        $employee->email = "sample@gmail.com";
        $employee->fill($data);
        $employee->save();
        return $employee;
    }

    /**
     * Display the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $user = Employee::findOrFail($id);
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $user = Employee::findOrFail($id);
        $data = $request->except(['created_at','deleted_at','updated_at','id']);
        $returnUser = $user->update($data);
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $user = Employee::findOrFail($id);
        $user->delete();
        return response(["deleted"]);
    }
}
