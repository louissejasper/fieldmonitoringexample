<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ClientsController extends Controller {

	public function __construct() {
		
	}

	public function index(Request $request){
		$clients = Client::paginate(10);
		return view('pages.clients.lists')
			->with(['clients'=>$clients]);
	}

	public function create(){
		$client = new Client;
		return view('pages.clients.create',compact('client'));
	}

	public function edit(Client $client){
		return view('pages.clients.update',compact('client'));
	}

	public function store(Request $request,Client $client){
		$data = $request->except(['_token','_method']);
		$client = $client->create($data);
		return redirect()->route('clients.preview',['client'=>$client]);
	}

	public function update(Request $request, Client $client){
		$data = $request->except(['_token','_method']);
		$client = $client->update($data);
		return redirect()->route('clients.preview',['client'=>$client]);
	}

	public function preview(Client $client){
		return view('pages.clients.preview')
		->with(['client' => $client]);
	}

	public function delete(Request $request, Client $client){
		$client->delete();
		return redirect()->route('clients.list');
	}

	
}
