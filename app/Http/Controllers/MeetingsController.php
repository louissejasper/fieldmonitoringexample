<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Client;
use App\Models\Employee;
use App\Models\Shift;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;

class MeetingsController extends Controller {

    protected $appointment;

    protected $request;

    public function __construct(Request $request, Appointment $appointment) {
        $this->request     = $request;
        $this->appointment = $appointment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Appointment $appointment) {
        $appointment->paginate();
        return view('pages.meetings.lists')
            ->with(['appointment' => $appointment]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Appointment $appointment) {

        return view('pages.meetings.create')
            ->with([
                'appointment' => $appointment,
                'shift'       => Shift::get(),
                'client'      => Client::get(),
                'member'      => $appointment::withSelectedMembers($appointment->id)->get(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Appointment $appointment) {
        $data        = $request->except(['_token', '_method', 'others', 'members']);
        $ownerField = ['user_id'=> \Auth::user()->id ,'viewed'=>Carbon::now(),'owner'=>Auth::id()];
        $members = array_merge([$ownerField], $request->members);
        $appointment = $appointment->create($data)->alongWithMembers($members);
        return redirect()->route('meetings.preview', [
            'appointment' => $appointment,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function preview(Request $request, Appointment $appointment) {
        return view('pages.meetings.preview')->with([
            'appointment' => $appointment,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Appointment $appointment) {
        return view('pages.meetings.update')->with([
            'appointment' => $appointment,
            'shift'       => Shift::get(),
            'client'      => Client::get(),
            'member'      => $appointment::withSelectedMembers($appointment->id)->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment) {
        $data        = $request->except(['_token', '_method', 'others', 'members']);
        $appointment->alongWithMembers($request->members)->update($data); 
        return redirect()->route('meetings.preview', [
            'appointment' => $appointment,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
}
