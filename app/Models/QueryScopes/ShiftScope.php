<?php

namespace App\Models\QueryScopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class ShiftScope implements Scope {

    protected $column;

    protected $direction;

    public function __construct($column, $direction = "DESC") {
        $this->column    = $column;
        $this->direction = $direction;
    }

    public function apply(Builder $builder, Model $model) {
        $builder->orderBy($this->column, $this->direction);
    }

}
