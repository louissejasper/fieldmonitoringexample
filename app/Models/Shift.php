<?php

namespace App\Models;

use App\Models\Appointment;
use Illuminate\Database\Eloquent\Model;
use App\Models\QueryScopes\ShiftScope;

class Shift extends Model {

    protected $table = 'shifts';

    protected $guard = ['*'];

    protected $fillable = ['start', 'end'];

    public function myShift() {
        return $this->hasMany(Appointment::class, 'shift_id');
    }

    public function changed(){
    	return $this->start .' - '. $this->end;
    }

    public static function orderByDesc(){
    	return self::oderBy('id','DESC');
    }
    
    protected static function boot() {
        parent::boot();
        static::addGlobalScope(new ShiftScope('start', 'ASC'));
    }
}
