<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentHistory extends Model {

	protected $table = 'appointment_histories';

	protected $fillable = [
		'appointment_id','status','modification'
	];

	public function histories(){
		$this->belongsTo(Appointment::class,'appointment_histories','appointment_id');
	}
}
