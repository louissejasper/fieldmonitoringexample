<?php

namespace App\Models;

use App\Models\Appointment;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model {

    use SoftDeletes;

    protected $table = 'clients';

    // protected $guard = ['id'];

    protected $fillable = ['whmcs_id', 'company', 'contact_person', 'contact_number', 'address', 'email', 'owner'];

    public function appointments() {
        /*in hasMany we need to be explicit to the appointment table foreign key or column*/
        return $this->hasMany(Appointment::class, 'client_id');
    }

    public function owner() {
        return $this->belongsTo(User::class, 'id');
    }

}
