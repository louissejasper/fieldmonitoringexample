<?php

namespace App\Models;

use App\Models\Appointment;
use App\Models\Position;
use App\Models\Unit;
use App\User as BaseUser;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends BaseUser {

    use SoftDeletes;

    protected $table = 'users';

    protected $fillable = [
        'emp_id', 'name', 'email', 'password', 'firstname', 'lastname', 'contact_number', 'image', 'address',
        'unit_id', 'position_id',
    ];

    public function appointment() {
        return $this->belongsTomany(Appointment::class, 'appointment_with', 'user_id', 'appointment_id');
    }

    public function positions() {
        return $this->belongsTo(Position::class, 'position_id');
    }

    public function units() {
        return $this->belongsTo(Unit::class, 'unit_id');
    }

    public function attachByPosition($position) {
        $this->positions()->associate($position)->save();
        return $this;
    }

    public function toNotify() {
        return is_null($this->positions->is_head) ? false : true;
    }
    
    public static function boot() {
        self::saving(function ($model) {
            $model->name = sprintf('%s %s',request()->input('firstname'),request()->input('lastname') );
        });
    }



}
