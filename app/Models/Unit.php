<?php

namespace App\Models;

use App\Models\Employee;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model {
    
    protected $table = 'units';

    protected $fillable = ['name','slug'];

    public function users(){
    	return $this->belongsToMany(User::class,'unit_user','user_id');
    }

    public function positions(){
    	return $this->hasMany(Position::class,'unit_id');
    }

    public function employees(){
    	return $this->hasMany(Employee::class,'unit_id');
    }

    public function attachByPosition($position){
    	$this->positions()->save($position);
    	return $this;
    }

    public function attachByEmployee($employee){
    	$this->employees()->save($employee);
    	return $this;
    }

    /*public function setSlugAttributes($value){
		$this->attributes['slug'] = request()->has('slug') ? str_slug(request('slug')):str_slug(request('name'));
	}*/

}
