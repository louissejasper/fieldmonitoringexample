<?php

namespace App\Models;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Model;

class Position extends Model{
    
    protected $table = 'positions';

    protected $fillable = ['name','slug','position_id','is_head'];
  
	public function units(){
		return $this->hasMany(Unit::class,'position_id');
	}

	public function employees(){
		return $this->hasMany(Employee::class,'user_id');
	}

}
