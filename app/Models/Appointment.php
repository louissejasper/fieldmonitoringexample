<?php

namespace App\Models;

use App\Models\Client;
use App\Models\Employee;
use App\Models\Shift;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model {

    use SoftDeletes;

    protected $table = 'appointments';

    // protected $guard = ['*'];

    // casting all the datetime data type to instanciate carbon
    protected $dates = [
        'schedule',
    ];

    protected $fillable = [
        'client_id', 'txn', 'schedule', 'location', 'purpose', 'date_start', 'date_end', 'departure', 'arrival', 'remark', 'status',
        'shift_id',
    ];

    public function clients() {
        /*client_id is the column in our table we need to be explicit in the current table*/
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function shift() {
        return $this->belongsTo(Shift::class, 'shift_id');
    }

    public function histories() {
        return $this->hasMany(AppointmentHistory::class, 'appointment_id');
    }

    public function owner() {
        return $this->belongsToMany(User::class, 'appointment_with', 'member_id');
    }

    public function alongWith() {
        return $this->belongsToMany(Employee::class, 'appointment_with', 'appointment_id', 'user_id')
                    ->withPivot('owner')
                    ->withTimestamps();
    }

    public function alongWithMembers($members = []) {
        $this->alongWith()->sync($members);
        return $this;
    }

    /**
     * { return a list of selected users in a appointment }
     * @todo Transfer it to queryScope class
     * @param  <type> $appointment_id The appointment identifier
     * @return <type> ( Illuminate\Database\Eloquent\Builder )
     */
    public static function withSelectedMembers($appointment_id = null) {
        $id                   = $appointment_id ?: 0;
        $currentUser          = \Auth::user()->id;
        $withSelectedMemebers = Employee::select('appointment_with.*', 'users.*')->where('id','<>',$currentUser)
            ->leftJoin('appointment_with', function ($join) use ($id) {
                $join->on('appointment_with.user_id', '=', 'users.id')
                ->on('appointment_with.appointment_id', '=', \DB::raw($id));
            });
        // dd($withSelectedMembers->toSql());
        return $withSelectedMemebers;
    }

    public function notify() {
        return $this->belongToMany(User::class, 'appoitnment_notification', 'manager_id');
    }

    public function setScheduleAttribute($value) {
        $this->attributes['schedule'] = Carbon::parse($value)->toDateTimeString();
    }

    public function setDepartureAttribute($value) {
        $this->attributes['departure'] = Carbon::parse($value)->format('G:i');
    }

    public function setArrivalAttribute($value) {
        $this->attributes['arrival'] = Carbon::parse($value)->format('G:i');
    }

}