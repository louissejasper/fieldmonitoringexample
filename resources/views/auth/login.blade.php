@extends('layouts.signin')

@section('content')
 <form id="sign_in" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="msg">Sign in to start your session</div>
        @if ($errors->has('email'))
            <span class="help-block error">
                <label>{{ $errors->first('email') }}</label>
            </span>
        @endif
          @if ($errors->has('password '))
            <span class="help-block error">
                <label>{{ $errors->first('password') }}</label>
            </span>
        @endif
     
        <div class="input-group form-group{{ $errors->has('email') ? 'error' : '' }}">
            <span class="input-group-addon">
                <i class="material-icons">person</i>
            </span>
            <div class="form-line">
                {{-- <input type="text" class="form-control" name="username" placeholder="Username" required autofocus> --}}
                <input id="email" type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>
            </div>
        </div>
        <div class="input-group form-group{{ $errors->has('password') ? ' error' : '' }}">
            <span class="input-group-addon">
                <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
                <input type="password" class="form-control" name="password" placeholder="Password" required>          
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8 p-t-5">
                <input type="checkbox" name="remember" id="rememberme" class="filled-in chk-col-pink {{ old('remember') ? 'checked' : '' }}">
                <label for="rememberme">Remember Me</label>
            </div>
            <div class="col-xs-4">
                <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
            </div>
        </div>
        
    </form>
@endsection
