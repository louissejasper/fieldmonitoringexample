<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Iconcept Global Advertisement</title>
    <!-- Favicon-->
    {{-- <link rel="icon" href="../../favicon.ico" type="image/x-icon"> --}}

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href='{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap/css/bootstrap.min.css') }}' rel="stylesheet" />

    <!-- Waves Effect Css -->
    <link href='{{ URL::asset('vendors/adminbsb-materialdesign/plugins/node-waves/waves.min.css') }}' rel="stylesheet" />

    <!-- Animation Css -->
    <link href='{{ URL::asset('vendors/adminbsb-materialdesign/plugins/animate-css/animate.css') }}' rel="stylesheet" />

    <!-- Custom Css -->
    <link href='{{ URL::asset('vendors/adminbsb-materialdesign/css/style.css') }}' rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href='{{ URL::asset('vendors/adminbsb-materialdesign/css/themes/all-themes.css') }}' rel="stylesheet" />
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">iConcept<b></b></a>
            <small>Field Monitoring System</small>
        </div>
        <div class="card">
            <div class="body">
               @yield('content')
            </div>
        </div>
    </div>



    <script src="{{ URL::asset('vendors/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/js/admin.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/js/pages/examples/sign-in.js') }}"></script>
</body>

</html>