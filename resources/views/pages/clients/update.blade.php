@extends('layouts.master')

@section('content')
<form action="{{ route('clients.update',[$client]) }}" method="POST">
    {{ method_field('PUT') }}
    @include('pages.clients.partials.forms',
        ["title"=> __('clients.title.preview') ,
         "formModel" => $client
    ])
    <div class="row">
        <div class="col-xs-3">
            <div class="form-group pull-right">
                <button class="btn bg-blue btn-lg btn-block waves-effect">
                    Save
                </button>
            </div>
        </div>
    </div>
</form>
@endsection()