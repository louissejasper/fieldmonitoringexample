@extends('layouts.master')
@section('pluginsStyles')
<!-- JQuery DataTable Css -->
<link href="{{URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection()
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/pages/meeting.css') }}">
@endsection()
@section('pluginsScripts')
@parent()
<!-- jquery table -->
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
@endsection()
@section('scripts')
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/js/pages/tables/jquery-datatable.js') }}"></script>

@endsection()

@section('content')
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    @lang('clients.title.list')
                </h2>
                
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ route('clients.create') }}">Create</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Company</th>
                            <th>Contact Person</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Company</th>
                            <th>Contact Person</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @forelse($clients as $client)
                            <tr data-id="{{ $client->id }}" class="" >
                                <td>{{ $client->company }}</td>
                                <td>{{ $client->contact_person }}</td>
                                <td>{{ $client->email }}</td>
                                <td>{{ $client->contact_number }}</td>
                            </tr>
                        @empty               
                    @endforelse
                    </tbody>
                </table>
                 {{ $clients->links() }}
            </div>

        </div>
    </div>
</div>
@endsection()
