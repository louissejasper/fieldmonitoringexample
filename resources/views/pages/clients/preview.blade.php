@extends('layouts.master')

@section('content')
<div class="row clearfix">
    <div class="col-xs-3">
        <div class="form-group pull-right">
            <a href="{{ route('clients.edit',['client'=>$client]) }}" class="btn bg-yellow btn-lg btn-block waves-effect">Update</a>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="form-group pull-right">
            <form action="{{ route('clients.delete',['client'=>$client]) }}" method="POST">
				 {{ method_field('DELETE') }}
				  {{ csrf_field() }}
            	<button class="btn bg-red btn-lg btn-block waves-effect">Delete</button>
            </form>
        </div>
    </div>
</div>
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>
					@lang('clients.title.preview')
				</h2>               
			</div>
			<div class="body">
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Company Name</h5>
								<p>{{ $client->company or '' }}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Contact Person</h5>
								<p>{{ $client->contact_person or '' }}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Email</h5>
								<p>{{ $client->email or '' }}</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Contact Number</h5>
								<p>{{ $client->contact_number or '' }}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Office Address</h5>
								<p>{{ $client->address or '' }}</p>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
@endsection()