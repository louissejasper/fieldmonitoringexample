@section('pluginsStyles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<!-- Wait Me Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">
<!-- Bootstrap Tagsinput Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<!-- Bootstrap Select Css -->
<link href="" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection()

@section('pluginsScripts')
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/momentjs/moment.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/autosize/autosize.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Select Plugin Js -->
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

@endsection()
@section('scripts')
<script src="{{ URL::asset('js/pages/clients/index.js') }}"></script>
@endsection()
 
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>
					{{ $title }}
				</h2>               
			</div>
			<div class="body">
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">

							<div class="form-line {{ !old('company',$formModel->company) ?: 'focused' }} ">
								<input type="text" value="{{ old('company',$formModel->company) }}" name="company" class="form-control">
								<label class="form-label">Company Name</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('contact_person',$formModel->contact_person) ?: 'focused' }} ">
								<input type="text" value="{{ old('contact_person',$formModel->contact_person) }}" name="contact_person" class="form-control">
								<label class="form-label">Contact Person</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('email',$formModel->email) ?: 'focused' }} ">
								<input type="text" value="{{ old('email',$formModel->email) }}" name="email" class="form-control">
								<label class="form-label">Email</label>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('contact_number',$formModel->contact_number) ?: 'focused' }} ">
								<input type="text" value="{{ old('contact_number',$formModel->contact_number) }}" name="contact_number" class="form-control">
								<label class="form-label">Contact Number</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('address',$formModel->address) ?: 'focused' }} ">
								<textarea class="form-control no-resize" name="address" row="3">{{ old('name',$formModel->address) }}</textarea>
								<label class="form-label">Office Address</label>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
{{ csrf_field() }}
<input type="hidden" value="{{ old('id',$formModel->id) }}" name="id" class="form-control">
