@extends('layouts.master')

@section('content')
<div class="row clearfix">
    <div class="col-xs-3">
        <div class="form-group pull-right">
            <a href="{{ route('employees.edit',['employee'=>$employee]) }}" class="btn bg-yellow btn-lg btn-block waves-effect">Update</a>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="form-group pull-right">
            <form action="{{ route('employees.delete',['employee'=>$employee]) }}" method="POST">
				 {{ method_field('DELETE') }}
				  {{ csrf_field() }}
            	<button class="btn bg-red btn-lg btn-block waves-effect">Delete</button>
            </form>
        </div>
    </div>
</div>
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>
					@lang('employees.title.preview') - {{ strtoupper($employee->name) }}
				</h2>               
			</div>
			<div class="body">
				<div class="row clearfix">
					<div class="col-sm-2">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Employee ID</h5>
								<p>{{ $employee->emp_id }}</p>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Fullname</h5>
								<p>{{ $employee->name }}</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Unit</h5>
								<p>{{ $employee->units->name or "Not applicable" }}</p>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Position</h5>
								<p>{{ $employee->positions->name or "Provision" }}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Email</h5>
								<p>{{ $employee->email }}</p>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Contact Number</h5>
								<p>{{ $employee->contact_number }}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<h5>Address</h5>
								<p>{{ $employee->address }}</p>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>
@endsection()