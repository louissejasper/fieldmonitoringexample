@section('pluginsStyles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<!-- Wait Me Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">
<!-- Bootstrap Tagsinput Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<!-- Bootstrap Select Css -->
<link href="" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection()

@section('pluginsScripts')
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/momentjs/moment.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/autosize/autosize.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Select Plugin Js -->
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

@endsection()
@section('scripts')
<script src="{{ URL::asset('js/pages/employees/form.js') }}"></script>
@endsection()

<div class="row clearfix">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>{{ $title }} - {{ $formModel->name }}</h2>               
			</div>
			<div class="body">
				<div class="row clearfix">
					<div class="col-sm-4">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('emp_id',$formModel->emp_id) ?: 'focused' }} ">
								<input type="text" value="{{ old('emp_id',$formModel->emp_id) }}" name="emp_id" class="form-control">
								<label class="form-label">Employee ID</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('firstname',$formModel->firstname) ?: 'focused' }} ">
								<input type="text" value="{{ old('firstname',$formModel->firstname) }}" name="firstname" class="form-control">
								<label class="form-label">Firstname</label>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('lastname',$formModel->lastname) ?: 'focused' }} ">
								<input type="text" value="{{ old('lastname',$formModel->lastname) }}" name="lastname" class="form-control">
								<label class="form-label">Lastname</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('email',$formModel->email) ?: 'focused' }} ">
								<input type="text" value="{{ old('email',$formModel->email) }}" name="email" class="form-control">
								<label class="form-label">Email</label>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('contact_number',$formModel->contact_number) ?: 'focused' }} ">
								<input type="text" value="{{ old('contact_number',$formModel->contact_number) }}" name="contact_number" class="form-control">
								<label class="form-label">Contact Number</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('address',$formModel->address) ?: 'focused' }} ">
								<textarea class="form-control no-resize" name="address" row="3">{{ old('address',$formModel->address) }}</textarea>
								<label class="form-label">Address</label>
							</div>
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>@lang('employees.title.details')</h2>               
			</div>
			<div class="body">
				<div class="row clearfix">
					<div class="col-xs-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('unit_id',$formModel->unit_id) ?: 'focused' }} ">
								<h5>Unit</h5>
								<select class="form-control show-tick" id="unit" name="unit_id">
									@forelse($unit->get() as $value)
										@php $isSelected = $value->id == $formModel->unit_id? 'selected':'' @endphp
										<option value="{{$value->id}}" {{$isSelected}}>{{$value->name}}</option>
									@empty
									@endforelse
								</select>							
							</div>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-xs-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('position_id',$formModel->position_id) ?: 'focused' }} ">
								<h5>Position</h5>
								<select class="form-control show-tick" id="position" name="position_id">
									@if($formModel->position_id)
										@foreach($formModel->units->positions as $position)
											@php $isSelected = $value->id == $formModel->unit_id? 'selected':'' @endphp
											<option value="{{$position->id}} {{$isSelected}}">{{$position->name}}</option>
										@endforeach
									@endif
								</select>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>
{{ csrf_field() }}
<input type="hidden" value="{{ old('id',$formModel->id) }}" name="id" class="form-control">