@extends('layouts.master')
@section('content')
<form id="employee" action="{{ route('employees.store') }}" method="POST">
    @include('pages.employees.partials.forms',[
        "title"=> __('employees.title.create'),
        "formModel" => $employee
    ])
    <div class="row">
        <div class="col-xs-3 pull-right">
            <div class="form-group">
                <button class="btn bg-blue btn-lg btn-block waves-effect">
                    Save
                </button>
            </div>
        </div>
    </div>
</form>
@endsection()