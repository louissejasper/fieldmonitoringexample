@extends('layouts.master')
@section('pluginsStyles')
<!-- JQuery DataTable Css -->
<link href="{{URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection()
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/pages/meeting.css') }}">
@endsection()
@section('pluginsScripts')
@parent()
<!-- jquery table -->
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
@endsection()
@section('scripts')
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/js/pages/tables/jquery-datatable.js') }}"></script>

@endsection()

@section('content')
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    @lang('employees.title.list')
                </h2>
                
                <ul class="header-dropdown m-r--5">
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">more_vert</i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="{{ route('employees.create') }}">Create</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive" >
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>EMP ID</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Unit</th>
                                <th>Position</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>EMP ID</th>
                                <th>Email</th>
                                <th>Name</th>
                                <th>Contact Number</th>
                                <th>Unit</th>
                                <th>Position</th>
                            </tr>
                        </tfoot>
                        <tbody>
                         @forelse($employees as $employee)
                             <tr>
                                <td>{{ $employee->emp_id }}</td>
                                <td>{{ $employee->email }}</td>
                                <td>{{ $employee->name }}</td>
                                <td>{{ $employee->contact_number }}</td>
                                <td>{{ $employee->units->name }}</td>
                                <td>{{ $employee->positions->name }}</td>
                            </tr>
                         @empty
                         <tr>
                            <td>Add an employee</td>
                        </tr>
                        @endforelse
                     </tbody>
                 </table>
                 {{ $employees->links() }}
             </div>
            
         </div>
     </div>
 </div>
</div>
@endsection()
