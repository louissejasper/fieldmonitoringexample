@extends('layouts.master')

@section('content')
<div class="row clearfix">
    <div class="col-lg-2 col-md-2 col-sm-5 col-xs-5">
        <div class="form-group pull-right">
            <a href="{{ route('meetings.edit',['appointment'=>$appointment]) }}" class="btn bg-blue btn-lg btn-block waves-effect">Update</a>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            @lang('meetings.title.preview') - <b class="font-underline col-pink">{{ $appointment->clients->contact_person }}</b>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row ">
                            <div class="col-sm-12 m-b-5">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Company:</h5>
                                        {{ $appointment->clients->company }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="form-line">
                                        <h5>Schedule:</h5>
                                        {{ $appointment->schedule->toDayDateTimeString() }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Status:</h5>
                                        {{ $appointment->status }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Depature Time</h5>
                                        {{ $appointment->departure }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Arrival Time</h5>
                                        {{ $appointment->arrival }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Meeting Place</h5>
                                        {{ $appointment->location }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Purpose</h5>
                                        {{ $appointment->purpose}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                       <h5>Purpose</h5>
                                        {{ $appointment->purpose}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           @lang('meetings.title.info')
                        </h2>               
                    </div>
                    <div class="body">      
                        <div class="row ">
                            <div class="col-sm-12 m-b-5">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Change Shift:</h5>
                                        {{ $appointment->shift->changed() }}
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group form-float form-group-md">
                                    <div class="form-line">
                                        <h5>Along with the FF:</h5>
                                        @foreach($appointment->alongWith as $member)
                                            <p>{{ $member->name }}</p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           @lang('meetings.title.history')
                        </h2>               
                    </div>
                    <div class="body">      
                        <div class="row">
                            <div class="col-sm-12">
                                @forelse($appointment->histories->take(30) as $history)
                                    <p>{{$history->status}} <small class="pull-right">{{$history->created_at}}</small></p>
                                @empty
                                    <p>No History found</p>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>


@endsection()
