@section('pluginsStyles')
<!-- Bootstrap Material Datetime Picker Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
<!-- Wait Me Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/waitme/waitMe.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">
<!-- Bootstrap Tagsinput Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet">
<!-- Bootstrap Select Css -->
<link href="" rel="stylesheet" />
<!-- Bootstrap Select Css -->
<link href="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
@endsection()

@section('pluginsScripts')
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/momentjs/moment.js') }}"></script>
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/autosize/autosize.js') }}"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
<!-- Select Plugin Js -->
<script src="{{ URL::asset('vendors/adminbsb-materialdesign/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

@endsection()
@section('scripts')
<script src="{{ URL::asset('js/pages/meetings/index.js') }}"></script>
@endsection()

<div class="row clearfix">
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>{{ $title }}</h2>
			</div>
			<div class="body">   	
				<div class="row ">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('client_id',$formModel->client_id) ?: 'focused' }}">
								<select class="form-control" name="client_id"  title="Select client" data-live-search="true">
									@forelse($client as $value)
										@php $isSelected = $value->id == $formModel->client_id ? 'selected':''  @endphp
										<optgroup label="{{ $value->company }}"  {{ $isSelected }}>
											<option value="{{$value->id}}" {{ $isSelected }}>
												{{ $value->contact_person }}
											</option>
										</optgroup>
										@empty
										<option>No Client Found</option>
									@endforelse
								</select>
								{{-- <label class="form-label">Client</label> --}}
							</div>
						</div>
					</div>
				</div>
				<div class="row ">
					<div class="col-sm-7">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('schedule',$formModel->schedule) ?: 'focused' }}">
								<input type="text" value="{{ old('schedule',$formModel->schedule) }}" name="schedule" class="form-control datepickermin ">
								<label class="form-label">Schedule</label>
							</div>
						</div>
					</div>
					@if($formModel->id)
					<div class="col-sm-5">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('status',$formModel->status) ?: 'focused' }}">
								<select class="form-control show-tick" name="status">
									<option disabled="">Update the status</option>
									@forelse(__('appointment.forms.status') as $status)
										@php  $isSelected = $formModel->status == $status ? 'selected':'' @endphp
											<option value="{{$status}}" {{ $isSelected }}> {{$status}} </option>	
										@empty
										<option value="">No Status Found</option>	
									@endforelse
								</select>
							</div>
						</div>
					</div>
					@endif
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('departure',$formModel->departure) ?: 'focused' }}">
								<input type="text" value="{{ old('departure',$formModel->departure) }}" name="departure" class="form-control datepickerhronly ">
								<label class="form-label">Departure Time</label>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('arrival',$formModel->arrival) ?: 'focused' }}">
								<input type="text" value="{{ old('arrival',$formModel->arrival) }}" name="arrival" class="form-control datepickerhronly ">
								<label class="form-label">Arrival Time</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('location',$formModel->location) ?: 'focused' }}">
								<textarea class="no-resize form-control" name="location" rows="3">{{ old('return',$formModel->location) }}</textarea>
								<label class="form-label">Meeting Place</label>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7">
						<div class="form-group form-float form-group-md">
							<div class="form-line {{ !old('purpose',$formModel->purpose) ?: 'focused' }}">
								<select class="form-control" name="purpose">
									<option value="" disabled="" selected="">Purpose?</option>
									<option value="iteration">Iteration</option>
									<option value="sales meeting">Sales Meeting</option>
									<option value="tcm">TCM</option>
									<option value="turnover">Turnover</option>
									<option value="tutorial">Tutorial</option>
									<option value="other">Other</option>
								</select>								
							</div>
						</div>
					</div>
					<div class="col-sm-5">
						<div class="form-group form-float form-group-md">
							<div class="form-line">
								<input type="text" value="" name="others" class=" form-control" >
								<label class="form-label">Others</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div class="card">
			<div class="header">
				<h2>@lang('meetings.title.details')</h2>               
			</div>
			<div class="body">      
				<div class="row ">
					<div class="col-sm-12">
						<div class="form-group form-float form-group-md">
							<h2 class="card-inside-title">Change Shift:</h2>
							<select class="form-control show-tick" name="shift_id">
								@forelse($shift as $value )
									@php $isSelected = $value->id == $formModel->shift_id ? 'selected':''  @endphp
									<option value="{{$value->id}}" {{ $isSelected }}>
										{{$value->start}} - {{$value->end}}
									</option>
								@empty
									<option value="">No Shift Found</option>								
								@endforelse
							</select>
						</div>
					</div>
				</div>
				<div class="row ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group form-float form-group-md">
							<h2 class="card-inside-title">With:</h2>
							<select class="form-control selectpicker" name="members[]" multiple ttile="Members">
								@forelse($member as $employee)
									@php $isSelected = ( !is_null($employee->user_id) ? 'selected':'')  @endphp
									<option value="{{ $employee->id}}" {{$isSelected}}>
										{{ $employee->name }}
									</option>
								@empty
									<option>Create a Members</option>
								@endforelse
							</select>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
{{ csrf_field() }}
<input type="hidden" value="{{ old('id',$formModel->id) }}" name="id" class="form-control">