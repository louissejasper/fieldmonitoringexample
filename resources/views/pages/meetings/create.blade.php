@extends('layouts.master')

@section('content')
<form id="employee" action="{{ route('meetings.create') }}" method="POST" >
    @include('pages.meetings.partials.forms',[
        "title"=> __('meetings.title.create'),
        "formModel" => $appointment
    ])
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 pull-right">
            <div class="form-group">
                <button class="btn bg-blue btn-lg btn-block waves-effect">
                    Save
                </button>
            </div>
        </div>
    </div>
</form>
@endsection()
