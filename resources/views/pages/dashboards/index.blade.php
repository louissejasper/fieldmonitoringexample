@extends('layouts.master')
@section('content')
<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">
                    playlist_add_check
                </i>
            </div>
            <div class="content">
                <div class="text">
                    NEW TASKS
                </div>
                <div class="number count-to" data-fresh-interval="20" data-from="0" data-speed="15" data-to="125">
                    125
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-cyan hover-expand-effect">
            <div class="icon">
                <i class="material-icons">
                    help
                </i>
            </div>
            <div class="content">
                <div class="text">
                    NEW TICKETS
                </div>
                <div class="number count-to" data-fresh-interval="20" data-from="0" data-speed="1000" data-to="257">
                    257
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-light-green hover-expand-effect">
            <div class="icon">
                <i class="material-icons">
                    forum
                </i>
            </div>
            <div class="content">
                <div class="text">
                    NEW COMMENTS
                </div>
                <div class="number count-to" data-fresh-interval="20" data-from="0" data-speed="1000" data-to="243">
                    243
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-orange hover-expand-effect">
            <div class="icon">
                <i class="material-icons">
                    person_add
                </i>
            </div>
            <div class="content">
                <div class="text">
                    NEW VISITORS
                </div>
                <div class="number count-to" data-fresh-interval="20" data-from="0" data-speed="1000" data-to="1225">
                    1225
                </div>
            </div>
        </div>
    </div>
</div>
@endsection()
