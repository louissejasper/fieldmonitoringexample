<?php

return [
    'title'   => [
        'list'    => 'My Employees',
        'create'  => 'Add a new Employee',
        'preview' => 'Detail of',
        'info'    => 'Other options',
        'details' => 'Other Details',
        'history' => 'Histories'
    ],
    'success' => 'Success fully save',
];