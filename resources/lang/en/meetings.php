<?php

return [
    'title'   => [
        'list'    => 'My appointments',
        'create'  => 'My Appointment',
        'preview' => 'Appointment with ',
        'update'  => 'Update Appointment Detail',
        'info'    => 'Other options',
        'details' => 'Other Details',
        'history' => 'Histories'
    ],
    'success' => 'Success fully save',
];