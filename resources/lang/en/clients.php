<?php

return [
    'title'   => [
        'list'    => 'My Clients',
        'create'  => 'Add a Client',
        'preview' => 'Client Details',
        'info'    => 'Other options',
        'details' => 'Other Details',
        'history' => 'Histories'
    ],
    'success' => 'Success fully save',
];