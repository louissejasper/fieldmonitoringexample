<?php

return [
    'title'   => [
        'list'    => 'My Clients',
        'create'  => 'Add a Client',
        'preview' => 'Client Details',
        'info'    => 'Other options',
        'details' => 'Other Details',
        'history' => 'Histories'
    ],
    'forms'=>[
    	"status" => ["Pending","Cancelled","Completed"]
    ],
    'storage' => 'images/qrcodes',
    'success' => 'Success fully save',
];