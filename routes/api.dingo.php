<?php
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->post('authenticate',
        ['middleware' => 'cors',
         'prefix' => 'v1',
         'uses' => 'App\Http\Controllers\Api\Auth\LoginController@authenticate']);

    $api->group(['namespace' => 'App\Http\Controllers\Api', 'prefix' => 'v1', 'middleware' => ['cors', 'api.auth']], function ($api) {
        $api->get('employee', 'UserController@index');
        $api->get('employee/{id}/user', 'UserController@show');
        $api->post('employee/store', 'UserController@store');
        $api->put('employee/{id}/user', 'UserController@update');
        $api->delete('employee/{id}', 'UserController@destroy');
    });

});