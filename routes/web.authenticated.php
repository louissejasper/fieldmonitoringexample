<?php 


Route::name('dashboards')->get('/home', 'DashboardsController@index');

/**
 * Meeting Controllers
 */
Route::name('meetings.list')->get('/meetings', [
    'uses' => 'MeetingsController@index',
]);

Route::name('meetings.create')->get('/meetings/create', [
    'uses' => 'MeetingsController@create',
]);

Route::name('meetings.store')->post('/meetings/create', [
    'uses' => 'MeetingsController@store',
]);

Route::name('meetings.preview')->get('/meetings/{appointment}/preview', [
    'uses' => 'MeetingsController@preview',
]);

Route::name('meetings.edit')->get('/meetings/{appointment}/edit', [
    'uses' => 'MeetingsController@edit',
]);

Route::name('meetings.update')->put('/meetings/{appointment}/update', [
    'uses' => 'MeetingsController@update',
]);

Route::name('meetings.delete')->delete('/meetings/{appointment}/delete', function () {
    return view('pages.meetings.lists');
});

/**
 * Clients Controllers
 */
Route::name('clients.list')->get('/clients', [
    'uses' => 'ClientsController@index',
]);

Route::name('clients.create')->get('/clients/create', [
    'uses' => 'ClientsController@create',
]);

Route::name('clients.store')->post('/clients/store', [
    'uses' => 'ClientsController@store',
]);

Route::name('clients.edit')->get('clients/{client}/edit', [
    'uses' => 'ClientsController@edit',
]);

Route::name('clients.update')->put('/clients/{client}/update', [
    'uses' => 'ClientsController@update',
]);

Route::name('clients.preview')->get('/clients/{client}/preview', [
    'uses' => 'ClientsController@preview',
]);

Route::name('clients.delete')->delete('/clients/{client}/delete', [
    'uses' => 'ClientsController@delete',
]);

/**
 * Employees Controllers
 */
Route::name('employees.list')->get('/employees', [
    'uses' => 'EmployeesController@index',
]);

Route::name('employees.create')->get('/employees/create', [
    'uses' => 'EmployeesController@create',
]);

Route::name('employees.store')->post('/employees/create', [
    'uses' => 'EmployeesController@store',
]);

Route::name('employees.preview')->get('/employees/{employee}/preview', [
    'uses' => 'EmployeesController@preview',
]);

Route::name('employees.edit')->get('/employees/{employee}/edit', [
    'uses' => 'EmployeesController@edit',
]);

Route::name('employees.update')->put('/employees/{employee}/update', [
    'uses' => 'EmployeesController@update',
]);

Route::name('employees.delete')->delete('/employees/{employee}/delete', [
    'uses' => 'EmployeesController@delete',
]);

Route::name('positions.ajax.get')->get('positions/{units}',[
    'uses'=> 'PositionsController@getPositions'
]);