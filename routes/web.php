<?php
Auth::routes();
Route::get('/','Auth\LoginController@showLoginForm');
Route::name('signout')->get('signout','Auth\LoginController@logout');

Route::name('accounts')->get('/accounts', function () {
    return view('layouts.master');
});

Route::group(['prefix' => 'how/'], function () {
    Route::get('/carbon', function () {
        $setDateTime = "3:00 PM";
        // $setTime = strtotime("04:25 PM");
        $getDateTime = \Carbon\Carbon::parse($setDateTime)->format('G:i');
        // ::parse('09-02-2017 09:58 PM')->toDateTimeString();
        dump($getDateTime);
        // $sample = $getDateTime::setToStringFormat('Y-m-d h:i A');
        // $shifts = App\Models\Shift::get();
        
        // dd($appointment->shift()->get());
    });

    Route::get('appointment-with-shift',function(){
        $appointment = new App\Models\Appointment;
        $shift = App\Models\Shift::get();
    });

    Route::get('query-scope',function(){
        $shifts = \App\Models\Shift::get();
        foreach($shifts as $shift):
            dump($shift->changed());
        endforeach;
    });
    
    Route::get('custom-query-with-employee',function(){
        $appointment = new App\Models\Appointment;
        dd($appointment::withSelectedMembers(1)->get());
    });

    Route::get('qrcode',function(){
        $url = Illuminate\Support\Facades\Storage::disk('public')->url("qrcode/1.png");
       
        echo '<img src="'.$url.'" />';
    });

   Route::get('histories',function(){
        $appointment = App\Models\Appointment::find(1);
        // dump($appointment->histories);
        dd(Auth::user()->id);
   });

   Route::get('attaching',function(){
        // $position = App\Models\Unit::find(1)->attachByPosition(['slug' => 'developer', 'name' => 'Developer']);
        $emp = App\Models\Employee::find(1);
        $position = App\Models\Position::find(3);
        $wtf =  $emp->attachByPosition($position);
        dd($wtf->save());
   });

   Route::get('factories',function(){
        // factory(App\User::class,1)->create();
        $positions = App\Models\Position::get()->random(1)->first();
        dump($positions->id);
   });

});


