<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>



## Installation requirement

> git clone this repo 

- before install dependency make sure you have a token in github for downloading the dingo/api which is path to other repo

>composer install

- setup the database configuration 

> php artisan vendor:publish --provider="Dingo\Api\Provider\LaravelServiceProvider"

- the above code will create a config file named api.php