"use strict";

(function ($) {
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
    });
    $.baseUrl = $('meta[name="base-url"]').attr('content');
})(jQuery);