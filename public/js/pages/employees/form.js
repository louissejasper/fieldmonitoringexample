(function($) {
    autosize($('textarea.auto-growth'));

    $('.datepickermin').bootstrapMaterialDatePicker({
        format: 'DD/MM/YYYY HH:mm A',
        disabledDays: [0, 6],
        minDate: new Date()
    });
    $('.datepickerhronly').bootstrapMaterialDatePicker({
        format: 'LT',
        shortTime: true,
        clearButton: true,
        date: false
    });
    moment()

    $('.selectpicker').selectpicker({
        liveSearch:true
    });

    console.log($('form#employee'));

    $('#unit').change(function(){
    	
    	var selectPosition = $('input[name="position"]');
    	
    	$.ajax({
    		url: $.baseUrl+'/positions/'+$(this).val(),
    		type: 'GET',
    	})
    	.done(function(data) {
    		var options = '';
    		for(var x in data){
    			options += `<option value="${data[x].id}">${data[x].name}</option>`;
    		}
    		$('#position').empty().html(options)
    		.selectpicker('refresh');
    	})
    	.fail(function() {
    		console.log("error");
    	});
    	
    });
})(jQuery);