(function($) {
    autosize($('textarea.auto-growth'));

    $('.datepickermin').bootstrapMaterialDatePicker({
        format: 'DD-MM-YYYY hh:mm A',
        disabledDays: [0, 6],
        shortTime: true,
        minDate: new Date()
    });
    $('.datepickerhronly').bootstrapMaterialDatePicker({
        format: 'LT',
        shortTime: true,
        clearButton: true,
        date: false
    });
    moment()

    $('.selectpicker').selectpicker({
        liveSearch:true
    });

})(jQuery);